import { createRouter, createWebHashHistory, createWebHistory } from 'vue-router'

// @ts-ignore
// @ts-ignore
const router = createRouter({
    history: createWebHashHistory('/'),
    routes: [
        {
            path: '/',
            name: 'home',
            component: () => import('../views/home.vue'),
            children: [
                {
                    //首页
                    path: '/',
                    name: 'main',
                    component: () => import('../views/global/index.vue'),
                },
                {
                    // 弹窗测试
                    path: '/margin',
                    name: 'margin',
                    component: () => import('../views/margin/index.vue'),
                },
                {
                    // 弹窗测试
                    path: '/synthesis',
                    name: 'synthesis',
                    component: () => import('../views/synthesis/index.vue'),
                },

            ]
        },
        {
            path: '/phoneHome',
            name: 'phoneHome',
            component: () => import('../views/home2.vue'),
            children: [
                {
                    path: '/',
                    redirect: '/phone'
                },
                {
                    // 手机首页
                    path: '/phone',
                    name: 'phone',
                    meta: {
                        keepAlive: true,
                        requireAuth: true
                    },
                    component: () => import('../views/indexPhone/index.vue'),
                },
                {
                    // 弹窗测试
                    path: '/find',
                    name: 'find',
                    meta: {
                        keepAlive: true,
                        requireAuth: true
                    },
                    component: () => import('../views/findPhone/findIndex.vue'),
                },
                {
                    // 弹窗测试
                    path: '/man',
                    name: 'man',
                    meta: {
                        keepAlive: true,
                        requireAuth: true
                    },
                    component: () => import('../views/man.vue'),
                },
            ]
        }
    ],
})
// router.beforeEach((to, from, next) => {
//     const playerUI = document.getElementById('playerUI');
//     if (playerUI) {
//         playerUI.style.display = 'none'; // 隐藏 #playerUI 元素
//     }
//     next();
// });
export default router


router.beforeEach(to => {
    console.log('to', to)
    if (to.path === '/man') {
        if (!localStorage.getItem('isManPage')) {
            localStorage.setItem('isManPage', '1')
            window.location.reload()
        }
    } else {
        if (localStorage.getItem('isManPage')) {
            localStorage.removeItem('isManPage')
            window.location.reload()
        }
    }
    return true
})
