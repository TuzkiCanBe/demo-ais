import { renderView, destroyView } from './createMyApp'

import objectPopUp from './objectPopUp.vue'


export const renderPop2 = (options = {}) => {
    const conf = {};
    const { app, view, instance } = renderView({
        view: objectPopUp,
        props: {
            ...options,
        },
        provide: {
            close: () => {
                destroyView(conf);
            }
        },
    })
    conf.app = app
    conf.view = view
    return instance
}
