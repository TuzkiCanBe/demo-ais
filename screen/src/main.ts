import { createApp } from 'vue'
import { createPinia } from 'pinia'
import "echarts-gl"
import App from './App.vue'
import router from './router'
import "amfe-flexible/index.js";
import './assets/fonts/font.css';
import './styles/root.scss';
import ElementPlus from 'element-plus';
import 'element-plus/theme-chalk/index.css';
import locale from 'element-plus/lib/locale/lang/zh-cn'
import dealUtil from 'lodash'//数据处理工具类
import 'vue3-video-play/dist/style.css' // 引入css
import './assets/fonts/font.css';


const app = createApp(App)
app.use(dealUtil)
app.use(createPinia())
app.use(router)
app.use(ElementPlus, { locale })

app.mount('#app')

