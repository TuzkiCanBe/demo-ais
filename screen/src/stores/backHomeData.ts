import {defineStore} from 'pinia'


// 主界面返回是否能返回
export const backStore = defineStore('backData', {
    state: () => ({// 其它配置项
        canback: true,
    }),

    actions: {//actions方法内部的this指向的是当前store
        saveData(bool: boolean) {
            this.canback = bool;
        },
    },

})