import {defineStore} from 'pinia'


// 视频是否能够播放
export const videoStore = defineStore('videoData', {
    state: () => ({// 其它配置项
        canPlay: true,
    }),

    actions: {//actions方法内部的this指向的是当前store
        saveData(bool: boolean) {
            this.canPlay = bool;
        },
    },

})