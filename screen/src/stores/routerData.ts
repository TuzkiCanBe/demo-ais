import {defineStore} from 'pinia'


// 第一个参数是应用程序中 store 的唯一 id
export const routeStore = defineStore('routeData', {
    state: () => ({// 其它配置项
        secRoute: {},//第二路径
        trdRoute: {},//第三路径
        forRoute: {},//第四路径
    }),

    actions: {//actions方法内部的this指向的是当前store
        saveSecRoute(object: any) {
            this.secRoute = object;
        },
        saveTrdRoute(object: any) {
            this.trdRoute = object;
        },
        saveForRoute(object: any) {
            this.forRoute = object;
        },
        clearAllRoute() {
            this.secRoute = {}
            this.trdRoute = {}
            this.forRoute = {}
        },
        clearlastTwoRoute() {
            this.trdRoute = {}
            this.forRoute = {}
        },
    },

})