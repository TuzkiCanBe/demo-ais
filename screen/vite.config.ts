import { fileURLToPath, URL } from 'node:url'

import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
//引入依赖
import autoprefixer from "autoprefixer"
// @ts-ignore
import postcsspxtorem from "postcss-pxtorem"

//获取当前日期函数
function getNowFormatDate() {
    let date = new Date(),
        year = date.getFullYear(), //获取完整的年份(4位)
        month: number | string = date.getMonth() + 1, //获取当前月份(0-11,0代表1月)
        strDate: number | string = date.getDate() // 获取当前日(1-31)
    if (month < 10) month = `0${month}` // 如果月份是个位数，在前面补0
    if (strDate < 10) strDate = `0${strDate}` // 如果日是个位数，在前面补0

    return `${year}-${month}-${strDate}`
}

export default defineConfig({
    base: '/',
    plugins: [vue()],
    resolve: {
        alias: {
            '@': fileURLToPath(new URL('./src', import.meta.url)),
        }
    },
    //配置适配方案
    css: {
        postcss: {
            plugins: [
                autoprefixer({
                    overrideBrowserslist: [
                        "Android 4.1",
                        "iOS 7.1",
                        "Chrome > 31",
                        "ff > 31",
                        "ie >= 8",
                        "last 10 versions", // 所有主流浏览器最近10版本用
                    ],
                    grid: true
                }),
                postcsspxtorem({
                    rootValue: 192, // 设计稿宽度的1/ 10 例如设计稿按照 1920设计 此处就为192
                    propList: ["*", "!border"], // 除 border 外所有px 转 rem
                    selectorBlackList: [".el-"], // 过滤掉.el-开头的class，不进行rem转换
                })
            ],
        },
    },
    build: {
        outDir: 'dist-' + getNowFormatDate()
    },

    server: {
        port: 3001,
        open: true, //自动打开
        host: '0.0.0.0',
        base: "./ ", //生产环境路径
        proxy: { // 本地开发环境通过代理实现跨域，生产环境使用 nginx 转发
            // 正则表达式写法
            '^/base': {
                target: 'https://wlyy.lx.gov.cn/wbviewapi/api/', // 后端服务实际地址
                changeOrigin: true, //开启代理
                rewrite: (path) => path.replace(/^\/base/, '')
            }
        }
    }

})






